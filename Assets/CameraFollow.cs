﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public Vector3 offset;
    public float speed;
    public float maxDistance;
    public float dist;
    public bool IsTargeting;

	void Start () {
        offset = transform.position;
	}

    private void FixedUpdate()
    {
        offset = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if(IsTargeting)
        transform.position = Vector3.Lerp(transform.position, offset, speed * 10f * Time.fixedDeltaTime);
    }
}
