﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {

    private Rigidbody2D rb;
    public float speedMult;
    public float speed;
    public GameObject kulkaPrefab;
    public Transform leftSzczelacz, rightSzczelacz;
    public float pociskSpeed;
    public float timeBtwShoot;
    public float startTimeBtsShoot;
    public float timeToLive;
    public int HP;
    public int MaxHP;
    public int Lives;
    public int LivesStart;
    public float minVelocity;
    public float maxDistance;
    public Transform target;
    public GameObject targetPrefab;
    private float distance;
    public float limitDistance;
    private float mapX = 170;
    private float mapY = 900;
    public float maxAngle;
    public float seekNewTargetDist;
    private Vector3 prevTarget;
    private Transform barrierLeft, barrierRight, barrierUp, barrierDown;
    public Status MyStatus;

    public enum Status
    {
        Roam, SpottedPlayer, Attacking, Retreat
    }

    // Use this for initialization
    void Start () {
        HP = MaxHP;
        rb = GetComponent<Rigidbody2D>();
        barrierUp = GameObject.Find("WallUp").transform;
        barrierDown = GameObject.Find("WallDown").transform;
        barrierLeft = GameObject.Find("WallLeft").transform;
        barrierRight = GameObject.Find("WallRight").transform;
        MyStatus = Status.Roam;
        InstantiateTarget();
    }
	
    void RoamUpdate()
    {
        distance = Vector3.Distance(transform.position, target.position);
        if (rb.velocity.magnitude < minVelocity)
        {
            rb.AddForce(Vector2.up * speed * Time.deltaTime);
        }
        if (distance < limitDistance)
        {
            SetTargetInTargetRange(seekNewTargetDist, true);
        }
        else
        {
            Vector2 force = new Vector2(-transform.position.x + target.position.x, -transform.position.y + target.position.y).normalized;
            rb.velocity = force * speed * Time.deltaTime;
            var dir = target.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
        }
    }

	// Update is called once per frame
	void Update () {
        switch (MyStatus)
        {
            case Status.Roam:
                RoamUpdate();
                break;
            default:
                DefaultUpdate();
                break;
        }
    }

    void DefaultUpdate()
    {

    }

    void InstantiateTarget()
    {
        var temp = Instantiate(targetPrefab, GameObject.Find("GameMaster").transform, true);
        target = temp.transform;
    }

    void SetNewRandomTarget()
    {
        float x = Random.Range(barrierLeft.transform.position.x * 0.9f, barrierRight.transform.position.x * 0.9f);
        float y = Random.Range(barrierUp.transform.position.y * 0.9f, barrierDown.transform.position.y * 0.9f);
        SetTarget(x, y);
    }

    void SetPlayerTarget()
    {
        Vector2 playerPos = GameObject.Find("Statek").transform.position;
        SetTarget(playerPos.x, playerPos.y);
    }

    void SetTargetInRange(float maxDistance, bool inFront = false)
    {
        var offset = Random.insideUnitCircle * maxDistance;
        var newLocation = new Vector2(transform.position.x + offset.x, transform.transform.position.y + offset.y);
        bool allow = true;
        if (inFront && !IsInAngleRange(newLocation)) allow = false;
        
        while(!IsInBounds(newLocation) && allow)
        {
            offset = Random.insideUnitCircle * maxDistance;
            newLocation = new Vector2(transform.position.x + offset.x, transform.transform.position.y + offset.y);
            allow = true;
            if (inFront && !IsInAngleRange(newLocation)) allow = false;
        }
        SetTarget(newLocation.x, newLocation.y);
    }

    void SetTargetInTargetRange(float maxDistance, bool inFront = false)
    {
        var offset = Random.insideUnitCircle * maxDistance;
        var newLocation = new Vector2(transform.position.x + offset.x, transform.transform.position.y + offset.y);
        offset = Random.insideUnitCircle * maxDistance;
        newLocation = new Vector2(target.position.x + offset.x, target.transform.position.y + offset.y);
        if (!IsInBounds(newLocation))
        {
            SetNewRandomTarget();
            return;
            //SetTarget(prevTarget.x, prevTarget.y);
            //Debug.Log("pszypal");
        }
        else SetTarget(newLocation.x, newLocation.y);
    }

    bool IsInAngleRange(Vector3 vector)
    {
        return (Vector3.Angle(transform.up, vector) < maxAngle);
    }

    void SetTarget(float x, float y)
    {
        if (target != null) prevTarget = new Vector3(target.position.x, target.position.y, target.position.z);
        target.position = new Vector3(x, y);
    }

    bool IsInBounds(Vector3 vector)
    {
        if (vector.x > barrierRight.transform.position.x || vector.x < barrierRight.transform.position.x || 
            vector.y > barrierUp.transform.position.y    || vector.y < barrierDown.transform.position.y)
        {
            //Debug.Log(vector.x + ", " + vector.y);
            return false;
        }
        else return true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rock"))
        {
            if (collision.relativeVelocity.magnitude > minVelocity)
                GetHit(1);
        }
    }

    public void GetHit(int amount)
    {
        HP -= amount;
        if (HP <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Lives--;
        Debug.Log("Umarneles po raz " + (LivesStart - Lives));
        Init();
    }

    private void Init()
    {
        Lives = LivesStart;
    }
}
