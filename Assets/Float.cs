﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour {

    private Rigidbody2D rb;
    public float speedMult;
    public float speed;
    public Vector2 distance;
    public Vector2 dest;
    public GameObject kulkaPrefab;
    public Transform leftSzczelacz, rightSzczelacz;
    public float pociskSpeed;
    public float timeBtwShoot;
    public float startTimeBtsShoot;
    public float timeBtwGetHit;
    public float startTimeGetHit;
    public float timeToLive;
    public int HP;
    public int MaxHP;
    public int Lives;
    public int LivesStart;
    public float minVelocity;
    public float maxDistance;

    // Use this for initialization
    void Start () {
        HP = MaxHP;
        rb = GetComponent<Rigidbody2D>();
        Camera.main.GetComponent<CameraFollow>().IsTargeting = true;
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        dest = new Vector2(transform.position.x - mousePos.x, transform.position.y - mousePos.y);
        transform.up = dest;
        distance = dest;
        if (Input.GetMouseButton(0))
        {
            //GameObject temp = Instantiate(kulkaPrefab, Random.Range(0, 1) == 0 ? leftSzczelacz : rightSzczelacz, true);
            if (timeBtwShoot <= 0)
            {
                GameObject temp = Instantiate(kulkaPrefab, Random.Range(0, 2) == 0 ? leftSzczelacz.position : rightSzczelacz.position, Quaternion.identity);
                temp.GetComponent<Rigidbody2D>().velocity = -dest.normalized * pociskSpeed;
                Destroy(temp, timeToLive);
                timeBtwShoot = startTimeBtsShoot;
            }
            else
                timeBtwShoot -= Time.deltaTime;
        }
        if(Input.GetMouseButtonDown(1))
        {
            Camera.main.GetComponent<CameraFollow>().IsTargeting = false;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            Camera.main.GetComponent<CameraFollow>().IsTargeting = true;
        }
        else if (Input.GetMouseButton(1))
        {
            dest *= speedMult;
        }
        //if(distance.magnitude < maxDistance)
        rb.velocity = -dest * speed;   
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Rock"))
        {
            if(collision.relativeVelocity.magnitude > minVelocity)
            {
                if (timeBtwShoot <= 0)
                {
                    timeBtwShoot = startTimeBtsShoot;
                    GetHit(1);
                }
                else
                {
                    timeBtwShoot -= Time.deltaTime;
                }
            }
        }
    }

    public void GetHit(int amount)
    {
        HP -= amount;
        if(HP <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Lives--;
        Debug.Log("Umarneles po raz "+ (LivesStart - Lives));
        Init();
    }

    private void Init()
    {
        Lives = LivesStart;
        Camera.main.transform.position = new Vector3(0, 0, Camera.main.transform.position.z);
        transform.position = Vector3.zero;
    }
    
}
