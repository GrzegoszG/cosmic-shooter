﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

    public GameObject tokenPrefab;
    public List<GameObject> rockPrefabs;
    public Transform parent;
    public List<GameObject> tokens;
    public List<GameObject> rocks;
    public int countS, countR;
    public Vector2 range;

    void Start () {
        GenerateStars();
        GenerateRocks();
	}


    void GenerateRocks()
    {
        for (int i = 0; i < countR; i++)
        {
            GenerateOneRock();
        }
    }

    void GenerateStars()
    {
        for(int i=0; i<countS; i++)
        {
            GenerateOne();
        }
    }

    void GenerateOne()
    {
        Vector3 location = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y));
        GameObject temp = Instantiate(tokenPrefab, location, Quaternion.identity);
        temp.transform.position = location;
        float scale = Random.Range(0.5f, 2f);
        temp.transform.localScale.Set(scale, scale, 1);
        temp.transform.SetParent(parent);
        tokens.Add(temp);
        temp = Instantiate(tokenPrefab, temp.transform);
        temp.transform.SetParent(parent);
        scale = Random.Range(0.5f, 2f);
        temp.transform.localScale.Set(scale, scale, 1);
        tokens.Add(temp);
    }

    void GenerateOneRock()
    {
        int index = Random.Range(0, rockPrefabs.Count);
        Vector3 location = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y));
        GameObject temp = Instantiate(rockPrefabs[index], transform);
        temp.transform.position = location;
        temp.transform.rotation.eulerAngles.Set(0, 0, Random.Range(0, 360));
        temp.GetComponent<Rigidbody2D>().velocity = Random.insideUnitCircle * Random.Range(0, 10f);
        temp.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(0, 10f);
        temp.GetComponent<SpaceRock>().generation = 0;
        float scale = Random.Range(0.5f, 2f);
        temp.transform.localScale.Set(scale, scale, 1);
        rocks.Add(temp);
    }

}
