﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public GameObject particlePrefab;
    public Transform parent;
    private static readonly string[] tags = { "Rock", "Limes" };
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        parent = GameObject.Find("GameMaster").transform;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.gameObject.CompareTag(tags[0]))
        {
            Instantiate(particlePrefab, collision.GetContact(0).point, Quaternion.identity, parent);
            Debug.Log(collision.GetContact(0).point);
        }
        else if(collision.transform.gameObject.CompareTag(tags[1]))
        {
            spriteRenderer.enabled = false;
            this.enabled = false;
        }
    }
}
