﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    public float startAlpha;
    public float baseAlpha;
    private CircleCollider2D col;
    private SpriteRenderer sr;
    public GameObject parent;

	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        col = GetComponent<CircleCollider2D>();
        SetAlpha(startAlpha);
        col.enabled = false;
	}
    
    public void Activate()
    {
        col.enabled = true;
    }

    void SetAlpha(float value)
    {
        value = Mathf.Clamp(value, 0, 1);
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, value);
    }
}
