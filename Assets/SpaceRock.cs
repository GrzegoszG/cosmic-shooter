﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceRock : MonoBehaviour {


    public List<GameObject> rocks;
    public int HP;
    public int MaxHP;
    public float spawnChance;
    public float childStartSpeed;
    public int generation = 0;
    public GameObject particeEffectPrefab;

    // Use this for initialization
    void Start() {
        Init();
    }

    public void Init()
    {
        HP = MaxHP;
        if (GetComponent<Collider2D>().enabled == false)
            Debug.Log("Disabled by Init");
    }

    public void GetHit(int amount)
    {
        HP -= amount;
        if (HP <= 0)
            DestroySelf();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        { 
        GetHit(1);
            if (GetComponent<Collider2D>().enabled == false)
                Debug.Log("Disabled by collision");
        }
    }

    void DestroySelf()
    {
        if (GetComponent<Collider2D>().enabled == false)
            Debug.Log("Disabled by destroy");
        float chance = Random.value;
        if(chance < spawnChance)
        {
            SpawnChild();
        }
        chance = Random.value + 0.05f + generation * 0.1f;
        if (chance < spawnChance || generation == 0)
        {
            SpawnChild();
        }
        chance = Random.value + 0.1f + generation * 0.1f;
        if (chance < spawnChance)
        {
            SpawnChild();
        }
        Destroy(gameObject, 0.01f);
    }

    void SpawnChild()
    {
        if (MaxHP == 1) return;
        var scale = Random.Range(0.5f, 0.8f) * (1 - generation * 0.1f);
        if (scale < 0.3) return;
        int childIndex = Random.Range(0, rocks.Count);
        GameObject child = Instantiate(rocks[childIndex], gameObject.transform);
        var rock = child.GetComponent<SpaceRock>();
        rock.MaxHP = this.MaxHP - 1;
        rock.Init();
        rock.generation = generation++;
        var offset = Random.insideUnitCircle * 2;
        child.transform.position = transform.position + new Vector3(offset.x, offset.y, 0);
        child.transform.parent = transform.parent;
        child.transform.localScale = new Vector3(scale, scale, 1);
        var rb = child.GetComponent<Rigidbody2D>();
        rb.velocity = Random.insideUnitCircle * childStartSpeed;
        if (child.GetComponent<Collider2D>().enabled == false)
            rock.Revive();
        Instantiate(particeEffectPrefab, child.transform.position, Quaternion.identity);
    }

    void Revive()
    {
        GetComponent<Collider2D>().enabled = true;
        GetComponent<SpaceRock>().enabled = true;
    }

    private void OnDisable()
    {
        if (GetComponent<Collider2D>().enabled == false)
            Debug.Log("Disabled by disable");
        Destroy(gameObject);
    }
}
