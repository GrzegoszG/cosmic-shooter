﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour {

    public float kreconkoSpeed;

	void Start () {

        CreateColor();
        kreconkoSpeed *= 10f * Random.Range(-1f, 1f);
    }
	
    void CreateColor()
    {
        var renderer = GetComponent<SpriteRenderer>();
        List<float> colors = new List<float>();
        colors.Add(0);
        colors.Add(1);
        colors.Add(Random.value);
        int index = 0;
        float r, g, b;

        index = Random.Range(0, colors.Count - 1);
        r = colors[index];
        colors.RemoveAt(index);

        index = Random.Range(0, colors.Count - 1);
        g = colors[index];
        colors.RemoveAt(index);

        index = Random.Range(0, colors.Count - 1);
        b = colors[index];
        colors.RemoveAt(index);

        renderer.color = new Color(r, g, b, Random.value);
    }

    void Update () {
        transform.Rotate(Vector3.forward, kreconkoSpeed * Time.deltaTime);
	}
}
